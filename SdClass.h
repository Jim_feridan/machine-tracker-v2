#include <SD.h>
#define MAXLOGSIZE 10485760 //10MB
#define DUMMYFILENAME "dummy file.txt"


class SdFunctions{

    private:
        short _fileIndex;
        String _fileName = "";
        const String _INFO = "[SD INFO] ";
        const String _ERROR = "[SD ERROR] ";
        const String _LOGEXTENSION = ".txt";
        const String _LOGBASENAME = "LogFile-";
    

    public:
        bool _hasError = true;
        String _status = "";

        void initialize(short);
        void checkExistingLogs(String);
        void writeLog(String);        
        bool isAppendable(void);
        bool isWritable(void);
        void makeFileName(void);

};

/* Initialize the SD card by giving it the CS/SS pin*/
void SdFunctions::initialize(short _csPin){

    if (!SD.begin(_csPin)) {
        _status = _ERROR + "SD card initialization failed, will continue in beacon mode";
        _hasError = true;
    }else{
        if (!isWritable()){ 
            _status = _ERROR + "SD card initialization failed, will continue in beacon mode";
            _hasError = true;
            return;
        }
        checkExistingLogs("/");
        _hasError = false;
        _status += ", SD card initialization succeeded";
    }
}

/*Iterate through files, get the file that is still appendable
otherwise make a new filename*/
void SdFunctions::checkExistingLogs(String dirPath){

    File rootDir = SD.open(dirPath);
    _fileIndex = 0;

    while(true){

        File eachFile = rootDir.openNextFile();

        if (!eachFile) { break; }
        if (!eachFile.isDirectory() && (String(eachFile.name()).indexOf(_LOGBASENAME) > -1)){
            Serial.println("is not dir");
            _fileIndex ++;
            if (MAXLOGSIZE - eachFile.size() > 0 &&
                String(eachFile.name()).indexOf(_LOGBASENAME) > -1){
                _fileName = eachFile.name();
                break;
            }
        }
        Serial.println("is not dir 2");
        eachFile.close();
    }

    if (_fileName == ""){
        makeFileName();
        _status = _INFO + "No file to continue, will create a new one: " + _fileName;
        File logFile = SD.open(_fileName, FILE_WRITE);
        logFile.close();
    }else{
        _status = _INFO + "Found a log file to continue: " + _fileName;
    }
}

void SdFunctions::writeLog(String logData){

    /* If there was an error in the SD card, deferred from writing to it*/
    if (_hasError){ 
        _status = _ERROR + "SD has error, deferring file write";
        return;
    }
    
    _status = _INFO;
    /* If the file is no longer writeable create new file*/
    if (!isAppendable()){
        _fileIndex ++;
        makeFileName();
        _status += "Created new logfile: " + _fileName + ". ";
    }
    
    /* Check again if file writeable, otherwise raise error*/
    if (!isAppendable()){
        _status = _ERROR + "Failed to write to file, SD might have a problem";
        _hasError = true;
    }else{
        _status += "Writen to log file: " + logData;
        _hasError = false;
        File logFile = SD.open(_fileName, FILE_WRITE);
        logFile.println(logData);
        logFile.close();
    }
}

/*--- Support functions ---*/

/* Check if it is possible to write on the file*/
bool SdFunctions::isAppendable(){
    
    File logFile = SD.open(_fileName, FILE_READ);
    if ( (!_hasError && logFile) && (logFile.size() <= MAXLOGSIZE)){
        logFile.close();
        return true;
    }else{
        return false;
    }
}

/* Check if the SD card is writable by creating a dummy file*/
bool SdFunctions::isWritable(){

    File dummyFile = SD.open(DUMMYFILENAME, FILE_WRITE);
    if (dummyFile){
        dummyFile.close();
        SD.remove(DUMMYFILENAME);
        return true;
    }else{
        return false;
    }
}

/* Make a file name for creating a logfile */
void SdFunctions::makeFileName(){
    _fileName = _LOGBASENAME + String(_fileIndex) + _LOGEXTENSION;
}
