/*
Author: Jenver I.

NodeMCU sketch for tracking a machine typically a vehicle
designed to sustain even in multiple(but not all) hardware module failures

*/
#include "nodemcu_pins.h"
#include "GpsClass.h"
#include "SdClass.h"
#include "ESP8266WiFi.h"
/* 
 * TODO:
 * Make the tolerance dynamic: add detection when engine is running*/
#define coorTolerance 0.0000471

bool isInvalidGPSDataReported = false;
bool isGPSModuleReported = false;
SdFunctions SDObject;
GpsFunctions GPSObject;

struct gpsCoord{
    String _status = "";
    float _lat = 0.0;
    float _lng = 0.0;
};

gpsCoord prevGpsData;

/*
 * TODO:
 * 
 * Add the GSM module for reporting
 * Add the wifi for debug and remote access
 * Socket function 1: get module status
 * Socket function 2: get reports from each hardware status
 */

void setup() {

    /* Wifi is not needed so turn it off*/
    WiFi.mode( WIFI_OFF );
    WiFi.forceSleepBegin();
    
    Serial.begin(9600);
    Serial.println("\n\nInitializing components");

    GPSObject.initialize();
    Serial.println(GPSObject._status);

    SDObject.initialize(SS);
    Serial.println(SDObject._status);

    Serial.println("\n--------------------------\n\n");
}

void loop() {

    GPSObject.readGpsSerial();
    GPSObject.updateGps();
    
    if (GPSObject._isNewGPSData){
        if (GPSObject._hasError && !isInvalidGPSDataReported){
            Serial.println(threshLog());
            isInvalidGPSDataReported = true;
        }else{
            Serial.println(threshLog());
            isInvalidGPSDataReported = false;
            isGPSModuleReported = false;
        }

        GPSObject._isNewGPSData = false;

    }else if (GPSObject.gpsModuleHasError() && !isGPSModuleReported){
        Serial.println(GPSObject._status);
        SDObject.writeLog(GPSObject._status);
        isGPSModuleReported = true;
    }
}

String threshLog(){

    bool isFar = false;
    char gpsReading[78];
    String appendedReading = "";
    String errorLog = "";
    
    sprintf(
        gpsReading,
        "%f, %f, %f, %f, %d",
        GPSObject._lat, 
        GPSObject._lng, 
        GPSObject._alt, 
        GPSObject._speed, 
        GPSObject._sat
    );

    appendedReading.concat(GPSObject._dateTime);
    appendedReading.concat(", ");
    appendedReading.concat(gpsReading);

    if (GPSObject._hasError){

        /* Check if error was previously recorded*/
        if (prevGpsData._status != GPSObject._status){
            errorLog.concat("[Main Error]================================>\n");
            errorLog.concat(GPSObject._status);
            errorLog.concat("\n");
            errorLog.concat(appendedReading);
            errorLog.concat("\n[Main Error]--------------------------------<");
            SDObject.writeLog(errorLog);
            prevGpsData._status = GPSObject._status;
            return errorLog;
        }

    }else{

        isFar = isFarPrevCoord( GPSObject._lat, GPSObject._lng, prevGpsData._lat, prevGpsData._lng);

        if (isFar){
            prevGpsData._lat = GPSObject._lat;
            prevGpsData._lng = GPSObject._lng;
            SDObject.writeLog(appendedReading);
            Serial.print("New log,  ");
        }else{
            Serial.print("Adjacent, ");
        }
    }

    return appendedReading;
}

bool isFarPrevCoord(float currLat, float currLng, float prevLat, float prevLng){
    return sqrt( pow((currLat - prevLat), 2) + pow((currLng - prevLng), 2))  > coorTolerance;
}
