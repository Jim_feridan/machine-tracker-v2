#include <TinyGPS++.h>
#include <TimeLib.h>
#include <SoftwareSerial.h>

#define MIN_YEAR 2020 //if date from gps is less, meaning its battery has been discharged
#define PH_UTC_OFFSET 8 //time zone correction
#define GPS_READ_INTERVAL 1000
#define TIME_TO_DECLARE_ERROR 3000
#define TEST_COUNT 3
#define BAUD_RATE 9600

TinyGPSPlus GPS;
SoftwareSerial GPSSerial(D2, D1);  // RX, TX

class GpsFunctions{

    private:
        unsigned long _lastGpsContact = 0;
        const String _INFO = "[GPS INFO] ";
        const String _ERROR = "[GPS ERROR] ";


    public:
        bool _isNewGPSData = false;
        bool _hasError = true;
        char _dateTime[19];
        String _status = "";
        float _lat = 0.0;
        float _lng = 0.0;
        float _alt = 0.0;
        float _speed = 0.0;
        short _sat = 0;

        void initialize();
        void readGpsSerial(void);
        bool gpsModuleHasError(void);
        void updateGps(void);

};

/* */
void GpsFunctions::initialize(){

    GPSSerial.begin(BAUD_RATE);
    for (short iteration = 0; iteration < TEST_COUNT; iteration++){
        readGpsSerial();
    }

    _hasError = gpsModuleHasError();
}

/* Read the input from serial for x amount of time to check if the module is responding*/
void GpsFunctions::readGpsSerial() {
     _isNewGPSData = false;
    for (unsigned long start = millis(); millis() - start < GPS_READ_INTERVAL;){
        while (GPSSerial.available()){
            if ( GPS.encode(GPSSerial.read()) ){ // Did a new valid sentence come in?
                _isNewGPSData = true;
                _lastGpsContact = millis();
            }
        }
    }
}

/* Check if the gps module has error by comparing the last time it responded*/
bool GpsFunctions::gpsModuleHasError(){
    
    _hasError = (millis() - _lastGpsContact >= TIME_TO_DECLARE_ERROR) || (GPS.charsProcessed() < 10);

    if (_hasError){
        _status = _ERROR + "GPS module not responding check hardware";
    }else{
        _status = _INFO + "GPS module is responding";
    }
    return _hasError;
}

void GpsFunctions::updateGps(){

    /* Check the validity of the crucial data*/
    if (!GPS.date.isValid() || !GPS.time.isValid() || GPS.date.year() < MIN_YEAR){
        
        _status = _ERROR + "Date time error";
        sprintf(
            _dateTime,
            "%03d",
            404
        );
        _hasError = true;
        return;
    }else{
        /* Construct the return value*/
        setTime(
            GPS.time.hour(),
            GPS.time.minute(),
            GPS.time.second(),
            GPS.date.day(),
            GPS.date.month(),
            GPS.date.year()
        );
        adjustTime(PH_UTC_OFFSET * 3600);
        
        sprintf(
            _dateTime,
            "%02d/%02d/%02d %02d:%02d:%02d",
            month(), day(), year(), hour(), minute(), second()
        );
    }

    if (GPS.location.isValid()){
      
        _lat = GPS.location.lat();
        _lng = GPS.location.lng();
    }

    _alt = GPS.altitude.isValid() ? GPS.altitude.meters() : 0.0;
    _speed = GPS.speed.isValid() ? GPS.speed.kmph() : 0.0;
    _sat = GPS.satellites.isValid() ? GPS.satellites.value() : 0;
    _status = _INFO + "New GPS data succesfully parsed";
    _hasError = false;
}
