# machine-tracker-v2

**Using nodemcu for tracking vehicle**

This project uses the Arduino IDE to upload source to NodeMCU please install the following:
1. Arduino IDE 1.8.15 or higher ([Download here](https://www.arduino.cc/en/software))


**Setup the Arduino IDE**
Prepare the Arduino IDE for the required libraries:
1. Add the ESP8266 board:

    - Go to _File > Preferences > Additional Boards Manager Urls_ then paste the ff: https://arduino.esp8266.com/stable/package_esp8266com_index.json
    - Go to _Tools > Board > Board manager_ then install **esp8266** by **ESP8266 Community**
    - Go to _Tools > Board > Esp8266 Boards_ then select the **Generic ESP8266 Boards**
    - Select the COM port later when the code is to be uploaded

1.  Install the TinyGPS++ library:
    - Download the zip file [here](https://codeload.github.com/mikalhart/TinyGPSPlus/zip/refs/tags/v1.0.2b)
    - On Arduino IDE go to _Sketch > Include library > Add .ZIP library..._ then select the downloaded zip file


**Additional libraries**
The project also uses the preinstalled libraries:
1. TimeLib
1. SoftwareSerial
1. SD


**Hardware used**

1. Micro SD card module
1. GPS Module (GY-NEO6MV2)
1. GSM Module (SIM 800L)
1. NodeMCU Esp12e

